// const express = require('express');
import express from 'express';
// const dotenv = require('dotenv');
import dotenv from 'dotenv';
// const products =  require('./data/products');
import productRoutes from './routes/productRoutes.js';
import connectDB from './config/db.js';
import {errorHandler, notFound} from './middlewares/errorMiddleware.js';
import colors from 'colors';
import userRoutes from './routes/userRoutes.js';
import orderRoutes from './routes/orderRoutes.js';
import uploadRoutes from './routes/uploadRoutes.js';
import path from 'path';


dotenv.config();

// connecting database
connectDB();


const app = express();
app.use(express.json()); // Request body parsing


app.use('/api/products', productRoutes);
app.use('/api/users', userRoutes);
app.use('/api/orders', orderRoutes);
app.use('/api/uploads', uploadRoutes);

//create a static folder
const __dirname = path.resolve();
app.use(`/uploads`, express.static(path.join(__dirname, '/uploads')));



if (process.env.NODE_ENV === 'production') {
	app.use(express.static(path.join(__dirname, '/frontend/build')));

	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, 'frontend', 'build', 'index.html'));
	});
} else {
	app.get('/', (req, res) => {
		res.send('API is running...');
	});
}


app.use(notFound);
app.use(errorHandler);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`server running in ${process.env.NODE_ENV} port ${PORT}`.gray.bold);
});