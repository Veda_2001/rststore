import express from 'express';
import { protect, admin } from '../middlewares/authMiddleware.js';
import { getProducts, getProductById, createProduct,
    deleteProduct,updateProduct, createProductReview } from '../Controllers/productController.js';

const router = express.Router();


router.route('/').get(getProducts).post(protect, admin, createProduct);
router
	.route('/:id')
	.get(getProductById)
	.delete(protect, admin, deleteProduct)
	.put(protect, admin, updateProduct);
router.route('/:id/reviews').post(protect, createProductReview);
export default router;
