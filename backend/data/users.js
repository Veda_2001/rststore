import bcrypt from 'bcryptjs';


const users = [
    {
        name: 'Admin',
        email: 'admin@exmaple.com',
        password: bcrypt.hashSync('123456', 10),
        isAdmin: true,
    },
    {
        name: 'Anya Smith',
        email: 'anaya@exmaple.com',
        password: bcrypt.hashSync('123456', 10),
        isAdmin: false,
    },
    {
        name: 'John Roy',
        email: 'johm@exmaple.com',
        password: bcrypt.hashSync('123456', 10),
        isAdmin: false,
    },

];

export default users;
